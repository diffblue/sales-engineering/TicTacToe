package com.diffblue.javademo;

interface MyCompare {

  public boolean compare(int num1, int num2);
}
